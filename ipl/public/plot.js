// generalized high chart fn :-

function highCharts(container, titleName, yAxisTitle, seriesName, seriesData) {
    return Highcharts.chart(container, {
        chart: {
            type: 'column'
        },
        title: {
            text: titleName
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: yAxisTitle
            }
        },
        legend: {
            enabled: false
        },

        series: [{
            name: seriesName,
            data: seriesData,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    })
}

// 1) matchesPlayedPerYear :-

function highChartMatchesPlayedPerYear() {
    fetch("http://localhost:3000/matchesPlayedPerYear.json")
        .then(response => response.json())
        .then(commits => {
            let dataArray = Object.entries(commits);

            highCharts('matchesPlayedPerYear', 'Number of Matches Played Per Year', 'Number of Matches', 'Matches', dataArray)
        });
}


// 2) matchesWonPerYear :-

function highChartMatchesWonPerYear() {
    fetch("http://localhost:3000/matchesWonPerYear.json")
        .then(response => response.json())
        .then(commits => {
            let years = Object.keys(commits);

            function findMatchArray(teamName) {
                let matchesArray = [];
                for (let obj of Object.values(commits)) {
                    matchesArray.push(obj[teamName])
                }
                return matchesArray
            }

            function findSeriesArrayOfObject(nestedObj) {
                let seriesArrayOfObject = [];
                let teamObj = {};
                for (let obj of Object.values(nestedObj)) {
                    for (let teamKey in obj) {
                        if (!(teamKey in teamObj)) {
                            teamObj[teamKey] = 0;
                        }
                    }
                }
                for (let teamKey in teamObj) {
                    let seriesObject = {};
                    seriesObject["name"] = teamKey;
                    seriesObject["data"] = findMatchArray(teamKey);
                    seriesArrayOfObject.push(seriesObject);
                }
                return seriesArrayOfObject
            }

            Highcharts.chart('matchesWonPerYear', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Matches Won Per Team Per Year in IPL'
                },
                xAxis: {
                    categories: years,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Matches'
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.05,
                        borderWidth: 0
                    }
                },
                series: findSeriesArrayOfObject(commits)

            });

        });

}


// 3) ExtraRuns :-

function highChartExtraRuns() {
    fetch("http://localhost:3000/extraRuns.json")
        .then(response => response.json())
        .then(commits => {

            let teamArray = Object.keys(commits);
            let runsArray = Object.values(commits);

            const chart = Highcharts.chart('extraRuns', {
                title: {
                    text: 'Extra Runs Conceded Per Team (2016)'
                },
                xAxis: {
                    categories: teamArray
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Extra Runs'
                    }
                },
                series: [{
                    type: 'column',
                    colorByPoint: true,
                    data: runsArray,
                    showInLegend: false
                }]
            });
        });
}


// 4) EconomicBowler :-

function highChartEconomicBowler() {

    fetch("http://localhost:3000/economicBowler.json")
        .then(response => response.json())
        .then(commits => {
            let economicBowlerArray = Object.entries(commits);

            highCharts('economicBowler', 'Top 10 Economical Bowlers (2015)', 'Economy rate', 'Economy Rate', economicBowlerArray)
        });
}

highChartMatchesPlayedPerYear()
highChartMatchesWonPerYear()
highChartExtraRuns()
highChartEconomicBowler()