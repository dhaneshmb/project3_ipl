import http from "http"
import path from "path"
import fs from "fs"
import url from "url"
const hostname = '127.0.0.1';
const port = 3000;

const __dirname = path.resolve();


const server = http.createServer((req, res) => {
    const reqUrl = req.url;
    const extension = reqUrl.split(".").splice(-1)[0];

    function fsReadFile(filePath, type) {
        fs.readFile(filePath, "utf8", function (err, data) {
            if (err) {
                res.writeHead(404);
                res.end(JSON.stringify(err));
                return;
            }
            res.writeHead(200, type);
            res.end(data);
        });
    }

    if (reqUrl === "/") {
        const htmlFilePath = path.join(__dirname, "/ipl/public/index.html");
        const htmlType = { 'Content-Type': "text/html" };
        fsReadFile(htmlFilePath, htmlType)

    } else if (reqUrl === "/plot.js") {
        const plotJsFilePath = path.resolve(__dirname, "ipl/public/plot.js");
        const jsType = { 'Content-Type': "application/javascript" };
        fsReadFile(plotJsFilePath, jsType)

    } else if (extension === "json") {
        let fileName = reqUrl.split("/").splice(-1)[0];
        const jsonFilePath = path.resolve(__dirname, `ipl/public/output/${fileName}`);
        const jsonType = { 'Content-Type': "application/json" };
        fsReadFile(jsonFilePath, jsonType)

    }
})

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
