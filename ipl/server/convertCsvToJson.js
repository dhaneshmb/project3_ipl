
import csv from "csvtojson";
import fs from "fs";
import path from "path";

const __dirname = path.resolve();

function convertCsvToJson(csvFilePath, jsonFilePath) {
    const absoluteCsvFilePath = path.resolve(__dirname, csvFilePath);
    csv()
        .fromFile(absoluteCsvFilePath)
        .then((jsonObj) => {
            const stringifyJsonObj = JSON.stringify(jsonObj);
            fs.writeFileSync(path.resolve(__dirname, jsonFilePath), stringifyJsonObj);
        })
}

convertCsvToJson("ipl/data/matches.csv", "ipl/data/matches.json");
convertCsvToJson("ipl/data/deliveries.csv", "ipl/data/deliveries.json")