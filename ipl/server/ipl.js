
// solution 1 :- 

function findMatchesPlayedPerYear(matchArrayOfObj) {

  let matchesPlayedPerYearObj = matchArrayOfObj.reduce((ObjToReturn, matchObj, index, matchArray) => {
    let year = matchObj["season"];
    if (year in ObjToReturn) {
      ObjToReturn[year]++;
    } else {
      ObjToReturn[year] = 1;
    }
    return ObjToReturn
  }, {});
  return matchesPlayedPerYearObj;
}


// solution 2 :-

function findMatchesWonPerYear(matchArrayOfObj) {

  let matchesWonPerYearObj = matchArrayOfObj.reduce((ObjToReturn, matchObj, index, matchArrayOfObj) => {
    let yearKey = matchObj["season"];
    let teamKey = matchObj["winner"];
    if (yearKey in objToReturn) {
      if (teamKey in objToReturn[yearKey]) {
        objToReturn[yearKey][teamKey]++;
      } else {
        if (teamKey !== "") {
          objToReturn[yearKey][teamKey] = 1;
        }
      }
    } else {
      objToReturn[yearKey] = {};
      objToReturn[yearKey][teamKey] = 1;
    }
    return objToReturn
  }, {});
  return matchesWonPerYearObj;
}


// solution 3 :-

function findExtraRuns(matchArrayOfObj, deliveriesArrayOfObj, year) {

  let matchIdArr = matchArrayOfObj.filter((matchObj) => {
    if (Number(matchObj["season"]) === year) {
      return matchObj
    }
  }).map((matchObj) => { return matchObj["id"] });

  let extraRunsObj = deliveriesArrayOfObj.reduce((objToReturn, deliveryObj, index, deliveriesArrayOfObj) => {
    let deliveryId = deliveryObj["match_id"];
    if (matchIdArr.includes(deliveryId)) {
      let team = deliveryObj["batting_team"];
      if (team in objToReturn) {
        objToReturn[team] += Number(deliveryObj["extra_runs"]);
      } else {
        objToReturn[team] = Number(deliveryObj["extra_runs"]);
      }
    }
    return objToReturn
  }, {})

  return extraRunsObj;
}


// solution 4 :-

function findEconomicBowler(matchArrayOfObj, deliveriesArrayOfObj, year) {

  function rate(runBallArr) {
    let run = runBallArr[0];
    let over = runBallArr[1] / 6;
    let rate = run / over;
    return rate
  }

  let matchIdArr = matchArrayOfObj.filter((matchObj) => {
    if (Number(matchObj["season"]) === year) {
      return matchObj
    }
  }).map((matchObj) => { return matchObj["id"] });

  let bowlersNameRunBallObj = deliveriesArrayOfObj.reduce((objToReturn, deliveryObj, index, deliveriesArrayOfObj) => {
    let deliveryId = deliveryObj["match_id"];
    if (matchIdArr.includes(deliveryId)) {
      let bowler = deliveryObj["bowler"];
      let run = Number(deliveryObj["total_runs"]);
      let ball = 1;
      if (bowler in objToReturn) {
        objToReturn[bowler][0] += run;
        objToReturn[bowler][1]++;
      } else {
        objToReturn[bowler] = [run, ball];
      }
    }
    return objToReturn
  }, {})

  let economicBowler2DArray = [];
  for (let bowlerNameKey in bowlersNameRunBallObj) {
    let economyRate = rate(bowlersNameRunBallObj[bowlerNameKey]);
    economicBowler2DArray.push([bowlerNameKey, economyRate])
  }

  economicBowler2DArray.sort((a, b) => a[1] - b[1]);

  let economicBowlerObj = economicBowler2DArray.slice(0, 10).reduce((objToReturn, economicBowlerArray) => {
    let bowlerName = economicBowlerArray[0];
    let economicRate = economicBowlerArray[1];
    objToReturn[bowlerName] = economicRate;
    return objToReturn
  }, {});

  return economicBowlerObj
}

// total runs obtained by any batsman in a particular year.

function findTotalRuns(matchArray, deliveriesArray, batsman, year) {

  let matchArrayId = matchArray.reduce((arrToReturn, matchObj) => {
    if (Number(matchObj["season"]) === year) {
      arrToReturn.push(matchObj.id)
    }
    return arrToReturn
  }, [])

  let totalRunsOfThePlayer = deliveriesArray.reduce((totalRuns, deliveryObj) => {
    if(matchArrayId.includes(deliveryObj["match_id"]) && deliveryObj["batsman"] === batsman) {
      totalRuns += Number(deliveryObj["total_runs"]);
    }
    return totalRuns
  }, 0)

  return totalRunsOfThePlayer
}


export {
  findMatchesPlayedPerYear,
  findMatchesWonPerYear,
  findExtraRuns,
  findEconomicBowler,
  findTotalRuns
}