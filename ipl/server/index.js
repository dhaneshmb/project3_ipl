import { Console } from "console";
import fs from "fs"
import path from "path"
import * as ipl from "./ipl.js"

const __dirname = path.resolve();

const matches = JSON.parse(fs.readFileSync(path.resolve(__dirname, "ipl/data/matches.json")));
const deliveries = JSON.parse(fs.readFileSync(path.resolve(__dirname, "ipl/data/deliveries.json")));


function calculateAndSave(data, jsonFileName) {
    let stringifyData = JSON.stringify(data);
    fs.writeFileSync(path.resolve(__dirname, `ipl/public/output/${jsonFileName}.json`), stringifyData);
}


calculateAndSave(ipl.matchesPlayedPerYear(matches), "matchesPlayedPerYear")
calculateAndSave(ipl.matchesWonPerYear(matches), "matchesWonPerYear")
calculateAndSave(ipl.extraRuns(matches, deliveries, 2016), "extraRuns")
calculateAndSave(ipl.economicBowler(matches, deliveries, 2015), "economicBowler")

  


